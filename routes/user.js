// User Router
const express = require('express');
const router = express.Router();

// Authentication
const auth = require('../auth');

// Controller
const UserController = require('../controllers/user');

// ROUTES

// To check if user's email already exists in our DB
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(result => res.send(result));
});

// To register new user
router.post('/', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
});

// Login via email
router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result));
});

// Login via Google
router.post('/verify-google-token-id', (req, res) => {
	UserController.verifyGoogleTokenId(req.body.tokenId).then(result => res.send(result));
});

// Fetching user's information
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	
	UserController.getDetails({ userId: user.id }).then(user => res.send(user));
})

// To add a category
router.post('/add-category', auth.verify, (req, res) => {
	const params = {
		type: req.body.type,
		name: req.body.name,
		userId: auth.decode(req.headers.authorization).id
	};
	
	UserController.addCategory(params).then(user => res.send(user));
})

// To add a record
router.post('/add-record', auth.verify, (req, res) => {
	const params = {
		type: req.body.type,
		name: req.body.name,
		description: req.body.description,
		amount: req.body.amount,
		userId: auth.decode(req.headers.authorization).id
	};
	
	UserController.addRecord(params).then(user => res.send(user));
})

module.exports = router;