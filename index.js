// Express
const express = require('express');
const api = express();

// Mongoose
const mongoose = require('mongoose');

// CORS
const cors = require('cors');
// const corsOptions = {
// 	origin: 'https://jp-budget-tracker-app.vercel.app/',
// 	optionsSuccessStatus: 200
// };
api.use(cors());
// app.options('*', cors());

// .env
require('dotenv').config();

// MongoDB Connection
const connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

mongoose.connection.once('open', () => {
	console.log('Database connection successful!')
});

// Middleware
api.use(express.json());
api.use(express.urlencoded({ extended: true }));

// Route
const userRoutes = require('./routes/user');
api.use('/api/users', userRoutes);

// Listening Port
const port = process.env.PORT || 4000;
api.listen(port, () => {
	console.log(`Listening on port ${ port }.`)
});