// User Schema
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	givenName: {
		type: String,
		required: [true, 'Given name is required.']
	},
	familyName: {
		type: String,
		required: [true, 'Family name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String
	},
	mobileNo: {
		type: String
	},
	loginType: {
		type: String,
		default: 'email'	// Alternative value is 'google'
	},
	categories : [
		{
			type: {
				type: String,
				required: [true, 'Category type is required.'] // Can only be "Income" or "Expense"
			},
			name: {
				type: String,
				required: [true, 'Category name is required.']
			}
		}
	],
	records: [
		{	// Added type and name of a category here so that the user can identify if the record is an expense or an income
			type: {
				type: String,
				required: [true, 'Category type is required.']
			},
			name: {
				type: String,
				required: [true, 'Category name is required.']
			},
			description: {
				type: String,
				required: [true, 'Record description is required.']
			},
			amount: {
				type: Number,
				required: [true, 'Record amount is required.']
			},
			createdOn: {
				type: Date,
				default: new Date()
			},
		}
	],
	budget: {
				type: Number // This will be the running budget for the user
			}
});

module.exports = mongoose.model('user', userSchema);